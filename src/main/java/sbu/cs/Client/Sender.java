package sbu.cs.Client;

import java.io.*;

class Sender {
    private final File file;
    private final byte[] byteArray;

    Sender(String filePath) {
        file = new File(filePath);
        byteArray = new byte[(int) file.length()];
    }

    void read() throws IOException {
        FileInputStream inputStream = new FileInputStream(file);
        BufferedInputStream bufferedStream = new BufferedInputStream(inputStream);
        bufferedStream.read(byteArray, 0, byteArray.length);
        bufferedStream.close();
        inputStream.close();
    }

    void send(DataOutputStream output) throws IOException {
        read();
        System.out.println("Sending file " + file.getPath() + " to server...");
        output.writeUTF(file.getPath());
        output.writeInt(byteArray.length);
        output.write(byteArray, 0, byteArray.length);
        output.flush();
        System.out.println("Done!");
        output.close();
    }
}
