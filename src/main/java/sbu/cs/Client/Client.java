package sbu.cs.Client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client {
    public static final String host = "localhost";
    public static final int port = 666;

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String filePath = args[0];      // "sbu.png" or "book.pdf"
        Socket client = new Socket(host, port);
        System.out.println("Connecting to server...");
        DataOutputStream output = new DataOutputStream(client.getOutputStream());
        Sender sender = new Sender(filePath);
        sender.send(output);
        client.close();
        System.out.println("Client shutdown!");
    }
}
