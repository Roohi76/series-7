package sbu.cs.Server;

import java.io.*;

class Receiver {
    private final String filePath;
    private final byte[] byteArray;

    Receiver(DataInputStream input) throws IOException {
        filePath = input.readUTF();
        byteArray = new byte[input.readInt()];
        input.readFully(byteArray);
        input.close();
    }

    void write(String directory) throws IOException {
        System.out.println("Receiving file " + filePath + " from client...");
        File file = new File(directory);
        if(!file.exists())
        {
            file.mkdir();
        }
        FileOutputStream outputStream = new FileOutputStream(directory + "/" + filePath);
        BufferedOutputStream bufferedStream = new BufferedOutputStream(outputStream);
        bufferedStream.write(byteArray, 0, byteArray.length);
        bufferedStream.flush();
        outputStream.flush();
        System.out.println("Done!");
        bufferedStream.close();
        outputStream.close();
    }
}
