package sbu.cs.Server;


import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final int port = 666;

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // below is the name of directory which you must save the file in it
        String directory = args[0];     // default: "server-database"
        ServerSocket server = new ServerSocket(port);
        System.out.println("Waiting for client to connect...");
        Socket client = server.accept();
        System.out.println("Client " + client + " connected!");
        DataInputStream input = new DataInputStream(client.getInputStream());
        Receiver receiver = new Receiver(input);
        receiver.write(directory);
        client.close();
        server.close();
        System.out.println("Server shutdown!");
    }
}
